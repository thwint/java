package ch.wintersoft.playground.jbbp.moxa;

import com.igormaznitsa.jbbp.JBBPParser;
import com.igormaznitsa.jbbp.io.JBBPByteOrder;
import com.igormaznitsa.jbbp.mapper.Bin;
import com.igormaznitsa.jbbp.mapper.BinType;
import ch.wintersoft.playground.jbbp.processor.MoxaFieldTypeProcessor;

public class MoxaIPReport {
    private static final String SKIP_2 = "skip:2;";
    public static final JBBPParser MOXA_PARSER = JBBPParser.prepare("" +
            "byte[4] header;" +
            "skip;" +
            "byte nameLength;" +
            "byte[nameLength] serverName;" +
            "skip:1;" +
            "byte hwLength;" +
            "<hex[hwLength] hwId;" +
            SKIP_2 +
            "mac macAddress;" +
            SKIP_2 +
            "<int serialNumber;" +
            SKIP_2 +
            "ipv4 ipAddress;" +
            SKIP_2 +
            "ipv4 netmask;" +
            SKIP_2 +
            "ipv4 gateway;" +
            SKIP_2 +
            "<fw firmware;" +
            "skip:1;" +
            "byte apLength;" +
            "<hex[apLength] apId;", new MoxaFieldTypeProcessor());

    @Bin(order = 1, type = BinType.BYTE_ARRAY, arraySizeExpr = "4")
    public String header;
    @Bin(order = 2, type = BinType.BYTE_ARRAY, arraySizeExpr = "9")
    public String serverName;
    @Bin(order = 3, byteOrder = JBBPByteOrder.LITTLE_ENDIAN)
    public String hwId;
    @Bin(order = 4, byteOrder = JBBPByteOrder.LITTLE_ENDIAN)
    public String apId;
    @Bin(order = 5)
    public String macAddress;
    @Bin(order = 6, byteOrder = JBBPByteOrder.LITTLE_ENDIAN)
    public int serialNumber;
    @Bin(order = 7, byteOrder = JBBPByteOrder.LITTLE_ENDIAN)
    public String ipAddress;
    @Bin(order = 8, byteOrder = JBBPByteOrder.LITTLE_ENDIAN)
    public String netmask;
    @Bin(order = 9, byteOrder = JBBPByteOrder.LITTLE_ENDIAN)
    public String gateway;
    @Bin(order = 10, byteOrder = JBBPByteOrder.LITTLE_ENDIAN)
    public double firmware;

    @Override
    public String toString() {
        return
               "MOXA IP Address report\n" +
               "######################\n" +
               "protocol header: " + header + "\n" +
               "Server name:     " + serverName + "\n" +
               "Hardware ID:     " + hwId + "\n" +
               "MAC Address:     " + macAddress + "\n" +
               "Serial Number:   " + serialNumber + "\n" +
               "IP address:      " + ipAddress + "\n" +
               "Netmask:         " + netmask + "\n" +
               "Gateway:         " + gateway + "\n" +
               "Firmware:        " + firmware + "\n" +
               "AP ID:           " + apId + "\n";
    }
}
