package ch.wintersoft.playground.jbbp.processor;

import com.igormaznitsa.jbbp.JBBPCustomFieldTypeProcessor;
import com.igormaznitsa.jbbp.compiler.JBBPNamedFieldInfo;
import com.igormaznitsa.jbbp.compiler.tokenizer.JBBPFieldTypeParameterContainer;
import com.igormaznitsa.jbbp.io.JBBPBitInputStream;
import com.igormaznitsa.jbbp.io.JBBPBitOrder;
import com.igormaznitsa.jbbp.io.JBBPByteOrder;
import com.igormaznitsa.jbbp.model.*;

import java.io.IOException;

/**
 * Custom field processor for MOXA IP report
 * <p>
 * See {@link JBBPCustomFieldTypeProcessor} for additional information
 * </p>
 * <p>
 * This processor converts custom fields from a Moxa IP report into a human readable format.
 * </p>
 * It accepts the following types:
 * <ul>
 *     <li>mac: A String representing a MAC address in the format AA-BB-CC-DD-EE-FF</li>
 *     <li>ipv4: A String representing an IPv4 address (also usable for netmask)</li>
 *     <li>hex: A string with concatenated hex values (e.g. 0A12)</li>
 *     <li>fw: A double containing the firmware version of the Moxa module</li>
 * </ul>
 */
public class MoxaFieldTypeProcessor implements JBBPCustomFieldTypeProcessor {
    private final String[] types = new String[]{"mac", "ipv4", "hex", "fw"};

    @Override
    public String[] getCustomFieldTypes() {
        return types;
    }

    @Override
    public boolean isAllowed(JBBPFieldTypeParameterContainer jbbpFieldTypeParameterContainer, String s, int i, boolean b) {
        return true;
    }

    @Override
    public JBBPAbstractField readCustomFieldType(JBBPBitInputStream in,
                                                 JBBPBitOrder bitOrder,
                                                 int parserFlags,
                                                 JBBPFieldTypeParameterContainer customTypeFieldInfo,
                                                 JBBPNamedFieldInfo fieldName,
                                                 int extraData,
                                                 boolean readWholeStream,
                                                 int arrayLength) throws IOException {


        switch (customTypeFieldInfo.getTypeName()) {
            case "mac":
                return new JBBPFieldString(fieldName, extractMacAddress(in, customTypeFieldInfo.getByteOrder()));
            case "ipv4":
                return new JBBPFieldString(fieldName, extractIpAddress(in, customTypeFieldInfo.getByteOrder()));
            case "hex":
                if (arrayLength < 1) {
                    throw new IllegalArgumentException("A hex String must have a certain length (e.g. hex[5]])!");
                } else {
                    return new JBBPFieldString(fieldName, extractHexString(in, arrayLength, customTypeFieldInfo.getByteOrder()));
                }
            case "fw":
                return new JBBPFieldDouble(fieldName, extractFirmware(in, customTypeFieldInfo.getByteOrder()));
            default:
                // Before reaching this point we should get a JBBPCompilationException
                throw new UnsupportedOperationException("No field type with name " + customTypeFieldInfo.getTypeName() + " is defined!");
        }
    }

    private String extractHexString(final JBBPBitInputStream in, int length, JBBPByteOrder byteOrder) throws IOException {
        StringBuilder sb = new StringBuilder(length * 2 + length - 1);
        byte[] bytes = in.readByteArray(length, byteOrder);
        for (byte aByte : bytes) {
            sb.append(String.format("%02X", aByte));
        }
        return sb.toString();
    }

    private String extractMacAddress(final JBBPBitInputStream in, JBBPByteOrder byteOrder) throws IOException {
        int length = 6;
        byte[] bytes = in.readByteArray(length, byteOrder);
        StringBuilder sb = new StringBuilder(length * 2 + length - 1);
        for (int i = 0; i < length; i++) {
            if (i > 0) {
                sb.append('-');
            }
            sb.append(String.format("%02X", bytes[i]));
        }
        return sb.toString();
    }

    private String extractIpAddress(final JBBPBitInputStream in, JBBPByteOrder byteOrder) throws IOException {
        int length = 4;
        byte[] bytes = in.readByteArray(length, byteOrder);
        StringBuilder sb = new StringBuilder(length * 2 + length - 1);
        for (int i = 0; i < length; i++) {
            if (i > 0) {
                sb.append('.');
            }
            sb.append(bytes[i] & 0xFF);
        }
        return sb.toString();
    }

    private double extractFirmware(final JBBPBitInputStream in, JBBPByteOrder byteOrder) throws IOException {
        int length = 4;
        byte[] bytes = in.readByteArray(length, byteOrder);
        StringBuilder sb = new StringBuilder(length + 1);
        for (int i = 0; i < length; i++) {
            sb.append(bytes[i]);
            if (i == 0) {
                sb.append(".");
            }
        }
        return Double.parseDouble(sb.toString());
    }
}
