package ch.wintersoft.playground.primitives;

import java.io.ByteArrayOutputStream;

public class BooleanToByteArray {

    /**
     * Convert all the given booleans to an array of bytes.
     * <p>
     * For every 8 boolean values one byte is created where the first parameter becomes the least significant bit (LSB)
     * </p>
     * Examples:
     * <ul>
     *     <li>convertBooleansToByteArray(true)=byte[]{1}</li>
     *     <li>convertBooleansToByteArray(false, true)=byte[]{2}</li>
     * </ul>
     * @param booleans boolean values to be converted to byte[]
     * @return Array of bytes representing the received boolean values
     */
    public byte[] convertBooleansToByteArray(boolean... booleans) {
        byte aByte = 0;
        final double bitCount = 8;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        for (int i = 0; i < booleans.length; i++) {
            aByte = (byte) ((aByte & 0xFF) | (booleans[i] ? 1 : 0) << (int) (i % bitCount));
            if ((i > 0 && (i + 1) % bitCount == 0) || i == booleans.length - 1) {
                bout.write(aByte);
                aByte = 0;
            }
        }
        return bout.toByteArray();
    }
}
